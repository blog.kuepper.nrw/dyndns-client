app_name = dyndns-remoteip

setup-mac:
	echo "brew install miniforge && \
	conda init zsh && \
	conda activate .venv && \
	pip install -r requirements.txt"

# conda create -n .venv python=3.9.2 && \

setup-linux:
	sudo pip install virtualenv
	virtualenv ../dyndns_client/
	source bin/activate
