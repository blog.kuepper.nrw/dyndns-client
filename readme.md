## prepare mac

   * brew install miniforge
   * conda init zsh
   * conda create -n .venv python=3.9.2 && \
   * conda activate .venv
   * pip install -r requirements.txt

## prepare linux

   * sudo pip install virtualenv
   * virtualenv ../dyndns_client/
   * source bin/activate

## config

   `cp env.example .env`

edit .env file

## run

   `python3 dyndns.py`

## cron

   `* * * * * /<install>/<path>/dyndns_client/dyndns.sh >> /<install>/<path>/dyndns_client/dyndns.log`
