import urllib.request
import urllib.parse
import requests
import os
from dotenv import load_dotenv

load_dotenv()

apikey = os.environ.get("apikey")
username = os.environ.get("username")
password = os.environ.get("password")

def getip():
    r = requests.get("https://dyndns.9it.eu/dyndns/remoteip")
    print(r.text)
    return r.text

ip = getip()

f = open("ip.txt", "r")
oldip = f.read()
# print(f.read())

if ip == oldip:
    print ("Up2date")
else:
    print ("Update DNS")
    f = open("ip.txt", "w")
    f.write(ip)
    f.close()
# url = 'https://dyndns.9it.eu/dyndns/update.pl?apikey=' + apikey + '&ip='+ip+'&user='+username+'&password='+password
# f = urllib.request.urlopen(url)
# print(f.read().decode('utf-8'))
